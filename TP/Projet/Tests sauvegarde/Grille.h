//
// Created by emmas on 24/11/2021.
//

#ifndef TP5_GRILLE_H
#define TP5_GRILLE_H
#include <iostream>
#include <fstream>
#include <vector>

class Grille {
public :
    Grille(int l, int c);
    void Affichage();
    int GetLignes();
    int GetColonnes();
    std::vector<std::vector<char>> GetGrille();

private:
    std::vector<std::vector<char>> _grille;
    int LIGNES;
    int COLONNES;

};


#endif //TP5_GRILLE_H
