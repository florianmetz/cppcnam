//
// Created by emmas on 24/11/2021.
//


#include "Grille.h"
using namespace  std;

Grille :: Grille (int l, int c){
    LIGNES=l;
    COLONNES=c;
    _grille.resize(LIGNES, std::vector<char>(COLONNES, 0));
    _grille[1][1]='A';
    _grille[2][1]='G';
}

void Grille::Affichage(){
    for (int i = 0; i < LIGNES; i++)
    {
        for (int j = 0; j < COLONNES; j++)
        {
            //Afficahge de la valeur
            if (_grille[i][j] == 0)
            {
                std::cout << " ";
            }
            else
            {
                std::cout << _grille[i][j];
            }

            //Affichage des colonnes
            if (j == COLONNES - 1)
            {
                std::cout << std::endl;
            }
            else
            {
                std::cout << " | ";
            }
        }
    }
}

int Grille::GetColonnes() {
    return COLONNES;
}

int Grille::GetLignes() {
    return LIGNES;
}

vector<vector<char>> Grille::GetGrille(){
    return _grille;
}