//
// Created by emmas on 06/12/2021.
//

#ifndef PROJET_SAUVEGARDE_H
#define PROJET_SAUVEGARDE_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "Grille.h"

using namespace std;


class Sauvegarde {
public :
    Sauvegarde();
    void lecture(Grille grille);
    void ecriture(Grille grille);

private:
    //string _fichier;

};

#endif //PROJET_SAUVEGARDE_H
