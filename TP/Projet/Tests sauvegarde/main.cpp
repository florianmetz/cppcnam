#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include "Sauvegarde.h"
#include "Grille.h"


using namespace std;



int main()
{
Grille g(3, 3);
g.Affichage();
Sauvegarde s;
s.ecriture(g);
s.lecture(g);
g.Affichage();
return 0;
}

