//
// Created by emmas on 06/12/2021.
//

#include "Sauvegarde.h"
using namespace  std;

Sauvegarde::Sauvegarde (){
}
void Sauvegarde ::lecture(Grille g) {
    ifstream fichier("test.txt");
    vector<vector<char>> grille=g.GetGrille();
    if(fichier)
    {

        string ligne; //Une variable pour stocker les lignes lues
        int numDebut=0;
        int numFin;
        int j=0;



        while( getline(fichier, ligne)) //Tant qu'on n'est pas à la fin, on lit
        {

            for (int i=0; i<ligne.length(); i++){
                if(i!=' '){
                    grille[j].push_back(ligne[i]);
                }
            }

            j++;
        }
    }
    else
    {
        cerr << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
    }
}


void Sauvegarde::ecriture(Grille grille)
{
    int num=3;
    string const nomFichier("test.txt");
    ofstream monFlux(nomFichier.c_str());

    if(monFlux)
    {
        monFlux<<num<<endl;
        for (int i=0; i<grille.GetLignes(); i++){
            for (int j=0; j<grille.GetColonnes(); j++){
                monFlux << grille[i][j];
            }
            monFlux<<endl;
        }
        monFlux<<num;
    }
    else
    {
        cerr << "ERREUR: Impossible d'ouvrir le fichier." << endl;
    }
}

