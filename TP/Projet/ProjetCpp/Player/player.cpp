#include "../Headers/player.h"

Player::Player(int id) :id(id)
{
}

Player::Player():id(0){};

int Player::getEnemyId(){
    if(id==1){
        return 2;
    }
    else{
        return 1;
    }
}

bool Player::operator==(Player const& p){
    return id==p.id;
}
