#ifndef GAMECHECKERS_H
#define GAMECHECKERS_H

#include "game.h"

class GameCheckers : public Game
{
public:
    GameCheckers(GridCheckers &_grid);
    void gameOngoing() override;
    bool caseChoose(const int player) override;
    bool gameEnding() override;
    void gameLoop(bool &fin) override;
    bool nextToEnemy(Box box, Player player);
    bool isPlayer(const Player player, const Box box);
    bool tokenSelectVerification(Box box, Player player);
    bool pawnMoveVerification(Box boxStart, Box boxEnd ,Player player);
    bool enemyBetween(const Box boxStart, const Box boxEnd, Player player);
    void captureEnemy(const Box boxEnemy, Player player);
    bool checkerMoveVerification(Box boxStart, Box boxEnd,Player player);

    bool putToken(int i, int j, int player);
    int playerVictory(std::string name1, std::string name2) const ;
    /*bool placeable(const int player);
    bool horizontalSurround(const int player, const int i, const int j, const bool flip) ;
    bool verticalSurround(const int player, const int i, const int j, const bool flip) ;
    bool diagonalSurround(const int player, const int i, const int j, const bool flip) ;*/
private:
    GridCheckers &grid;
    const int MAX_ERROR = 3;
};

#endif // GAMECHECKERS_H
