#ifndef GAMEOTHELLO_H
#define GAMEOTHELLO_H

#include "game.h"
#include "igameothello.h"

class GameOthello : public IGameOthello
{
public:
    GameOthello(GridOthello &_grid);
    void gameOngoing() override;
    bool caseChoose(const int player) override;
    bool gameEnding(const int player) override;
    void gameLoop(bool &fin) override;
    void testMaxError(int &player, bool &fin, int &count);

    bool putToken(int i, int j, int player);
    int playerVictory(std::string name1, std::string name2) const ;
    bool placeable(const int player);
    bool horizontalSurround(const int player, const int i, const int j, const bool flip) ;
    bool verticalSurround(const int player, const int i, const int j, const bool flip) ;
    bool diagonalSurround(const int player, const int i, const int j, const bool flip) ;
    bool isPlayer(const int player, const int i, const int j) ;
private:
    GridOthello &grid;
    const int MAX_ERROR = 3;
};

#endif // GAMEOTHELLO_H
