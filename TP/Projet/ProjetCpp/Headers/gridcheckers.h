#ifndef GRIDCHECKERS_H
#define GRIDCHECKERS_H

#include "grid.h"
#include "token.h"

class GridCheckers : public Grid
{
public:
    GridCheckers();
    void initCheckers();
    bool isPlayable(Box box);
};

#endif // GRIDCHECKERS_H
