#ifndef PLAYER_H
#define PLAYER_H

#include <string>

class Player
{
public:
    Player(int id);
    Player();
    int getEnemyId();

    int getId() const
    {
        return id;
    }

    const std::string getName() const
    {
        return name;
    }

    void setNom(const std::string& newName)
    {
        name = newName;
    }
bool operator==(Player const& p);


private:
    int id;
    std::string name;
};

#endif // PLAYER_H
