#ifndef BOX_H
#define BOX_H

#include <iostream>
#include <stdlib.h>

class Box
{
    public:
        Box(const int lin, const int col);
        int line;
        int column;
        bool operator==(Box const& b);
        bool operator!=(Box const& b);
        int operator-(Box const& b);
        bool operator>(Box const& b);
        bool operator<(Box const& b);
        bool enemyBetween(const Box b);
};

#endif // BOX_H
