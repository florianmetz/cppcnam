#ifndef GRIDOTHELLO_H
#define GRIDOTHELLO_H

#include "grid.h"

class GridOthello : public Grid
{
public:
    GridOthello();
    void initOthello();
};

#endif // GRIDOTHELLO_H
