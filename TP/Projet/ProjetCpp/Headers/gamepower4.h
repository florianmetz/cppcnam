#ifndef GAMEPOWER4_H
#define GAMEPOWER4_H

#include "game.h"

class GamePower4 : public Game
{
public:
    GamePower4(GridPower4 &_grid);
    void gameOngoing() override;
    bool caseChoose(const int player) override;
    bool gameEnding() override;
    void gameLoop(bool &fin) override;
    bool putToken(int j, int player) ;  //j numero de colonne dans laquelle on veut mettre un jeton
    bool completeDiagonal(int player) ;
    bool playerVictory(int player);
private:
    GridPower4 &grid;
};
#endif // GAMEPOWER4_H
