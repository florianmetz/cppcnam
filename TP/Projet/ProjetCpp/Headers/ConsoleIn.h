#ifndef CONSOLEIN_H
#define CONSOLEIN_H

#include <string>
#include <iostream>
#include <stdlib.h>
class ConsoleIn {
public :
    static ConsoleIn& getInstance()
    {
        static ConsoleIn instance; // Guaranteed to be destroyed.
        return instance;         // Instantiated on first use.
    }
    std::string StringInput();
    int IntInput();
    void TwoIntInput(int &first, int &second);
private:
    ConsoleIn(){}
    ConsoleIn(const ConsoleIn&) = delete;
    ConsoleIn& operator = (const ConsoleIn&) = delete;
};


#endif // CONSOLEIN_H
