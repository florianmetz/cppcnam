#ifndef TOKEN_H
#define TOKEN_H

#include <string>
#include "player.h"

class Token
{
public:
    Token(Player player, std::string type);
    Token(Player player);
    Token();
    bool operator==(const Token &t);

    bool isType(const std::string type);

   private:
    std::string type;
    Player player;
};

#endif // TOKEN_H
