#ifndef SAVE_H
#define SAVE_H

#include <iostream>
#include <fstream>
#include <vector>
#include "grid.h"
#include <QFile>
#include <QTextStream>
#include <QString>

using namespace std;

class Save
{
public:
    Save(Grid &_grid);
    void lecture();
    void ecriture();
private:
    Grid &grid;
    QString FILENAME = "SaveFile.txt";
};

#endif // SAVE_H
