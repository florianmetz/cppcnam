#ifndef GAMEMORPION_H
#define GAMEMORPION_H

#include "game.h"

class GameMorpion : public Game
{
public:
    GameMorpion(GridMorpion &_grid);
    void gameOngoing() override;
    bool caseChoose(const int player) override;
    bool gameEnding() override;
    void gameLoop(bool &fin) override;
    bool putToken(Box box, int player);
    bool completeDiagonal(int diagonal, int player);
    bool playerVictory(int player);

private:
    GridMorpion &grid;
};

#endif // GAMEMORPION_H
