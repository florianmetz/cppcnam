#ifndef IGAMEOTHELLO_H
#define IGAMEOTHELLO_H

#include "game.h"

class IGameOthello : public Game
{
public:
    IGameOthello(GridOthello &_grid);
    virtual bool gameEnding(const int player) = 0;
};

#endif // IGAMEOTHELLO_H
