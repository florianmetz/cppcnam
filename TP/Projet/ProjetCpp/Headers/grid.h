#ifndef GRID_H
#define GRID_H

#include <stdbool.h>
#include <vector>
#include <iostream>
#include "box.h"
#include "token.h"

class Grid
{
public:
    Grid(const int nbLine, const int nbColumn, const int nbTokenForVictory);
    void init();
    bool isEmpty(const Box box) const;
    bool isFull() const;
    void displayGrid() const;

    std::vector<std::vector<int>>& getgrid() {
        return grid;
    }
    void setgrid(const std::vector<std::vector<int>>& newgrid) {
        grid = newgrid;
    }

    int getNB_COLUMN() const {
        return NB_COLUMN;
    }
    int getNB_LINE() const {
        return NB_LINE;
    }
    int getNB_TOKEN_FOR_VICTORY() const{
        return NB_TOKEN_FOR_VICTORY;
    }
protected:
    std::vector<std::vector<int>> grid;
    const int NB_LINE;
    const int NB_COLUMN;
private:
    const int NB_TOKEN_FOR_VICTORY;

};

#endif // GRID_H
