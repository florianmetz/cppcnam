#ifndef GAME_H
#define GAME_H

#include "player.h"
#include "grid.h"
#include "gridmorpion.h"
#include "gridpower4.h"
#include "gridothello.h"
#include "gridcheckers.h"
#include <iostream>
#include <stdlib.h>
#include <string>
#include "ConsoleOut.h"
#include "ConsoleIn.h"
#include "box.h"


class Game
{
public:
    Game(Grid &_grid);
    void gameStarting();
    bool chooseVerification(const int i) const;
    void congratulations(const int winner) const;
    bool completeLine(const int i, const int player) const;
    bool completeColumn(const int j, const int player) const;
    virtual void draw();
    virtual void gameOngoing() = 0;
    virtual bool gameEnding();
    virtual bool caseChoose(const int player) = 0;
    virtual void gameLoop(bool &fin) = 0;

protected:
    Player j1 = Player(1);
    Player j2 = Player(2);
    const int NB_PLAYER = 2;
private:
    Grid &grid;
    int choice;

};


#endif // GAME_H
