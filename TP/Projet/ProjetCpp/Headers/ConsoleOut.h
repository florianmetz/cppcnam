//
// Created by emmas on 18/12/2021.
//

#ifndef CONSOLEOUT_H
#define CONSOLEOUT_H
#include "grid.h"
#include <string>

class ConsoleOut {
public :
    static ConsoleOut& getInstance()
    {
        static ConsoleOut instance; // Guaranteed to be destroyed.
        return instance;         // Instantiated on first use.
    }

    void WelcomeMessage();
    void Hello();
    void ErrorInput(int max);
    void ErrorColor(Player player);
    void ErrorBox();
    void ManualEnd();
    void CaseSelectTwoInput(const int player);
    void CaseChooseOneInput(const int player);
    void CaseChooseTwoInput(const int player);
    void ChooseName (const int player);
    void StartMessage ();
    void VictoryMessage(const std::string name);
    void GenericDraw();
    void Replay();
    void EndGame();
    void DisplayScoreOthello(const std::string name, const int score );
private:
    ConsoleOut(){}
    ConsoleOut(const ConsoleOut&) = delete;
    ConsoleOut& operator = (const ConsoleOut&) = delete;
};


#endif //PROJETCPP_CONSOLE_H
