#include "../Headers/gridcheckers.h"
#include "../Headers/ConsoleOut.h"
#include "../Headers/token.h"

GridCheckers::GridCheckers() : Grid(10, 10, 10)
{
    initCheckers();
}

void GridCheckers::initCheckers() {
    int nbLineInit=3;;

    for(int col=0; col<NB_COLUMN; col++){
        for (int line=0; line<nbLineInit; line++){
            if ((line%2==0 && col%2!=0) || (line%2!=0 && col%2==0)){
                grid[line][col]=1;
            }
        }

        for (int line =NB_LINE-nbLineInit;line<NB_LINE; line++){
            if ((line%2==0 && col%2!=0) || (line%2!=0 && col%2==0)){
                grid[line][col]=2;
            }
        }
    }
}

bool GridCheckers::isPlayable(Box box){
    int line=box.line;
    int col=box.column;
    if ((line%2==0 && col%2!=0) || (line%2!=0 && col%2==0)){
        return true;
    }
    return false;
}

