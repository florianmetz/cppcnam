#include "../Headers/gridothello.h"
#include "../Headers/ConsoleOut.h"

GridOthello::GridOthello() : Grid(8, 8, 10)
{
    initOthello();
}

void GridOthello::initOthello() {
    grid[3][3] = 1;
    grid[4][4] = 1;
    grid[3][4] = 2;
    grid[4][3] = 2;
}


