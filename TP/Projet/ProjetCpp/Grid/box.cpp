#include "../Headers/box.h"

Box::Box(const int lin, const int col)
{
    line=lin;
    column=col;
}

bool Box::operator==(Box const& b){
    if (column==b.column && line==b.line){
        return true;
    }
    return false;
}
bool Box::operator!=(Box const& b){
    if (column!=b.column || line!=b.line){
        return true;
    }
    return false;
}

//Différence de lignes
int Box::operator-(Box const& b){
    int diff=line-b.line;
    return diff;
}

bool Box::operator<(Box const& b){
    if(line<b.line ){
        return true;
    }
    else if (line==b.line && column<=b.column){
            return true;
}
    return false;
}

bool Box::operator>(Box const& b){
    if(line>b.line) {
        return true;
    }
    else if (line==b.line && column>=b.column){
            return true;
}
    return false;
}



