#include "../Headers/grid.h"

Grid::Grid(const int nbLine, const int nbColumn, const int nbTokenForVictory) :NB_LINE(nbLine), NB_COLUMN(nbColumn), NB_TOKEN_FOR_VICTORY(nbTokenForVictory)
{
    init();
}

void Grid::init() {
    grid.assign(NB_LINE, std::vector<int>(NB_COLUMN, 0));
}

bool Grid::isEmpty(const Box box) const
{
    int line=box.line;
    int col=box.column;
    if (line >= 0 && line < NB_LINE && col >= 0 && col < NB_COLUMN)
    {
        if (this->grid[line][col] == 0)
            return true;
    }
    return false;
}

bool Grid::isFull()const {
    for (int i = 0; i < NB_LINE; i++) {
        for (int j = 0; j < NB_COLUMN; j++) {
            if (this->grid[i][j] == 0) {
                return false;
            }
        }
    }
    return true;
}

void Grid::displayGrid() const
{
    std::cout << "\\ ";
    for (int j = 0; j < NB_COLUMN;j++) {
        std::cout << j << " ";
    }
    std::cout << std::endl;
    for (int i = 0; i < NB_LINE; i++) {
        std::cout << i << " ";
        for (int j = 0; j < NB_COLUMN; j++) {
            if (this->grid[i][j] == 0) {
                std::cout << "_ ";
            }
            else {
                std::cout << char(this->grid[i][j]) << " ";
            }
        }
        std::cout << std::endl;
    }
}


