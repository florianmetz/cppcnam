#include "../Headers/token.h"

Token::Token(Player _player, std::string _type):type(_type),player(_player)
{
}
Token::Token(Player _player):type("pawn"),player(_player)
{
}
Token::Token()
{
}

bool Token::operator==(const Token &t){
     return player==t.player && type==t.type;
}

bool Token ::isType(const std::string type){
    return this->type==type;
}
