#include "../Headers/gamemorpion.h"

GameMorpion::GameMorpion(GridMorpion &_grid) : Game(_grid), grid(_grid)
{
}

void GameMorpion::gameOngoing() {
    gameStarting();
    grid.displayGrid();

    bool fin = false;
    while (!fin) {
        gameLoop(fin);
    }
}

void GameMorpion::gameLoop(bool &fin){
    bool deposeJeton = false;
    for (int i = 1; i <= NB_PLAYER && !fin; i++) {
        while (!deposeJeton) {
            deposeJeton = caseChoose(i);
            if (!deposeJeton) {
                ConsoleOut::getInstance().ErrorInput(grid.getNB_LINE());
            }
        }
        grid.displayGrid();
        fin = this->gameEnding();
        deposeJeton = false;
    }
}

bool GameMorpion::caseChoose(const int player) {
    ConsoleOut::getInstance().CaseChooseTwoInput(player);
    int l ;
    int c ;
    ConsoleIn::getInstance().TwoIntInput(l, c);

    if (chooseVerification(l) && chooseVerification(c) && putToken(Box (l,c), player)) {
        return true;
    }
    return false;
}

bool GameMorpion::gameEnding() {
    if (playerVictory(1)) {
        congratulations(1);
        return true;
    }
    else if (playerVictory(2)) {
        congratulations(2);
        return true;
    }
    else if (grid.isFull()) {
        draw();
        return true;
    }
    else {
        return false;
    }
}


bool GameMorpion::putToken(Box box, int player)
{
    int line=box.line;
    int col=box.column;
    if (grid.isEmpty(box)) {
        grid.getgrid()[line][col] = player;
        return true;
    }
    return false;
}

bool GameMorpion::completeDiagonal(int diagonal, int player)
{
    if (diagonal == 0) {
        for (int i = 0; i < grid.getNB_LINE(); i++) {
            if (!(grid.getgrid()[i][i] == player))
                return false;
        }
    }
    else if (diagonal == 2) {
        int j = 2;
        for (int i = 0; i < grid.getNB_LINE(); i++) {
            if (!(grid.getgrid()[i][j] == player))
                return false;
            j--;
        }
    }
    else {
        return false;
    }
    return true;

}

bool GameMorpion::playerVictory(int player)
{
    for (int i = 0; i < grid.getNB_LINE(); i++) {
        if (completeColumn(i, player) || completeLine(i, player)) {
            return true;
        }
    }

    return (completeDiagonal(0, player) || completeDiagonal(2, player));
}


