#include "../Headers/gamepower4.h"

GamePower4::GamePower4(GridPower4 &_grid) : Game(_grid), grid(_grid)
{
}

void GamePower4::gameOngoing() {
    gameStarting();
    grid.displayGrid();

    bool fin = false;
    while (!fin) {
        gameLoop(fin);
    }
}

void GamePower4::gameLoop(bool &fin){
    bool deposeJeton = false;
    for (int i = 1; i <= NB_PLAYER && !fin; i++) {
        while (!deposeJeton) {
            deposeJeton = caseChoose(i);
            if (!deposeJeton) {
                ConsoleOut::getInstance().ErrorInput(grid.getNB_COLUMN());
            }
        }
        grid.displayGrid();
        fin = this->gameEnding();
        deposeJeton = false;
    }
}


bool GamePower4::caseChoose(const int player) {
    ConsoleOut::getInstance().CaseChooseOneInput(player);

    int c = ConsoleIn::getInstance().IntInput();

    if (chooseVerification(c) && putToken(c, player)) {
        return true;
    }
    return false;
}

bool GamePower4::gameEnding() {
    //si quelqu'un a gagné
    if (playerVictory(1)) {
        congratulations(1);
        return true;
    }
    else if (playerVictory(2)) {
        congratulations(2);
        return true;
    }
    //si la grid est pleine
    else if (grid.isFull()) {
        draw();
        return true;
    }
    else {
        return false;
    }
}


bool GamePower4::putToken(int j, int player) {
    for (int i = grid.getNB_LINE() - 1; i >= 0; i--) {
        if (grid.isEmpty(Box(i,j))) {
            grid.getgrid()[i][j] = player;
            return true;
        }
    }
    return false;
}

bool GamePower4::completeDiagonal(int player) {
    // diagonal ascendente
    for (int i = 3; i < grid.getNB_LINE(); i++) {
        for (int j = 0; j < grid.getNB_COLUMN() - 3; j++) {
            if (grid.getgrid()[i][j] == player && grid.getgrid()[i - 1][j + 1] == player && grid.getgrid()[i - 2][j + 2] == player && grid.getgrid()[i - 3][j + 3] == player)
                return true;
        }
    }
    // diagonal descendante
    for (int i = 3; i < grid.getNB_LINE(); i++) {
        for (int j = 3; j < grid.getNB_COLUMN(); j++) {
            if (grid.getgrid()[i][j] == player && grid.getgrid()[i - 1][j - 1] == player && grid.getgrid()[i - 2][j - 2] == player && grid.getgrid()[i - 3][j - 3] == player)
                return true;
        }
    }
    return false;
}


bool GamePower4::playerVictory(int player)
{
    for (int i = 0; i < grid.getNB_COLUMN(); i++) {
        if (completeColumn(i, player)) {
            return true;
        }
    }
    for (int i = 0; i < grid.getNB_LINE(); i++) {
        if (completeLine(i, player)) {
            return true;
        }
    }

    return (completeDiagonal(player));
}

