#include "../Headers/game.h"

Game::Game(Grid &_grid) :grid(_grid)
{
}

void Game::gameStarting() {
    ConsoleOut::getInstance().Hello();

    for (int i = 1; i <= NB_PLAYER; i++) {
        ConsoleOut::getInstance().ChooseName(i);
        std::string prenom = ConsoleIn::getInstance().StringInput();
        if (i == 1)
            j1.setNom(prenom);
        else
            j2.setNom(prenom);
    }
    ConsoleOut::getInstance().StartMessage();
}

bool Game::chooseVerification(const int i) const
{
    if (i<0 || i>this->grid.getNB_COLUMN()) {
        return false;
    }
    return true;
}

void Game::congratulations(const int winner) const
{
    if (winner == 1) {
        ConsoleOut::getInstance().VictoryMessage(j1.getName());
    }
    else {
        ConsoleOut::getInstance().VictoryMessage(j2.getName());
    }
}

void Game::draw()
{
    ConsoleOut::getInstance().GenericDraw();
    std::string saisie = ConsoleIn::getInstance().StringInput();

    if (saisie == "Y") {
        ConsoleOut::getInstance().Replay();
    }
    else {
        ConsoleOut::getInstance().EndGame();
    }
}

bool Game::gameEnding() {
    return false;
}


bool Game::completeLine(const int i, const int player) const
{
    int count = 0;
    for (int j = 0; j < grid.getNB_LINE(); j++) {
        if (grid.getgrid()[i][j] != player) {
            return false;
        }
        count++;
        if (count == grid.getNB_TOKEN_FOR_VICTORY()) {
            return true;
        }
    }
    return true;
}

bool Game::completeColumn(const int j, const int player) const
{
    int count = 0;
    for (int i = 0; i < grid.getNB_COLUMN(); i++) {
        if (grid.getgrid()[i][j] != player) {
            return false;
        }
        count++;
        if (count == grid.getNB_TOKEN_FOR_VICTORY()) {
            return true;
        }
    }
    return true;
}

