#include "../Headers/gameothello.h"

GameOthello::GameOthello(GridOthello &_grid) : IGameOthello(_grid), grid(_grid)
{
}

void GameOthello::gameOngoing() {
    grid.initOthello();
    gameStarting();
    grid.displayGrid();

    bool fin = false;
    while (!fin) {
        gameLoop(fin);
    }
}

void GameOthello::gameLoop(bool &fin){
    bool deposeJeton = false;
    for (int player = 1; player <= NB_PLAYER && !fin; player++) {
        int count = 0;
        while (!deposeJeton) {
            deposeJeton = caseChoose(player);
            if (!deposeJeton) {
                ConsoleOut::getInstance().ErrorInput(grid.getNB_LINE());
                count++;
                testMaxError(player, fin, count);
            }
        }

        grid.displayGrid();
        fin = gameEnding(0);
        deposeJeton = false;
    }
}

void GameOthello::testMaxError(int &player, bool &fin, int &count){
    if (count > MAX_ERROR)
    {
        ConsoleOut::getInstance().ManualEnd();
        int var = ConsoleIn::getInstance().IntInput();
        if (var == 1)
        {
           fin = gameEnding(player);
        }
    }
}

bool GameOthello::caseChoose(const int player) {
    ConsoleOut::getInstance().CaseChooseTwoInput(player);

    int l;
    int c;
    ConsoleIn::getInstance().TwoIntInput(l, c);

    if (chooseVerification(l) && chooseVerification(c) && putToken(l, c, player)) {
        return true;
    }
    return false;
}

bool GameOthello::gameEnding(const int player) {
    if (grid.isFull() || player!=0) {
        Player p1 = j1;
        std::string name1 = p1.getName();
        Player p2 = j2;
        std::string name2 = p2.getName();
        congratulations(playerVictory(name1, name2));

        return true;
    }
    return false;
}


bool GameOthello::placeable(const int player){
    int ennemi;
    if (player == 1) {
        ennemi = 2;
    }
    else {
        ennemi = 1;
    }

    for (int i = 0; i < grid.getNB_LINE(); i++) {
        for (int j = 0; j < grid.getNB_COLUMN(); j++) {
            if (grid.getgrid()[i][j] == ennemi) {
                if (grid.isEmpty(Box(i - 1, j - 1))) {
                    if (diagonalSurround(player, i - 1, j - 1, false) ||
                        horizontalSurround(player, i - 1, j - 1, false) ||
                        verticalSurround(player, i - 1, j - 1, false)) {
                        return true;
                    }
                }
                if (grid.isEmpty(Box(i - 1, j))) {
                    if (diagonalSurround(player, i - 1, j, false) ||
                        horizontalSurround(player, i - 1, j, false) ||
                        verticalSurround(player, i - 1, j, false)) {
                        return true;
                    }
                }
                if (grid.isEmpty(Box(i - 1, j + 1))) {
                    if (diagonalSurround(player, i - 1, j + 1, false) ||
                        horizontalSurround(player, i - 1, j + 1, false) ||
                        verticalSurround(player, i - 1, j + 1, false)) {
                        return true;
                    }
                }
                if (grid.isEmpty(Box(i, j - 1))) {
                    if (diagonalSurround(player, i, j - 1, false) ||
                        horizontalSurround(player, i, j - 1, false) ||
                        verticalSurround(player, i, j - 1, false)) {
                        return true;
                    }
                }
                if (grid.isEmpty(Box(i, j + 1))) {
                    if (diagonalSurround(player, i, j + 1, false) ||
                        horizontalSurround(player, i, j + 1, false) ||
                        verticalSurround(player, i, j + 1, false)) {
                        return true;
                    }
                }
                if (grid.isEmpty(Box(i + 1, j - 1))) {
                    if (diagonalSurround(player, i + 1, j - 1, false) ||
                        horizontalSurround(player, i + 1, j - 1, false) ||
                        verticalSurround(player, i + 1, j - 1, false)) {
                        return true;
                    }
                }
                if (grid.isEmpty(Box(i + 1, j))) {
                    if (diagonalSurround(player, i + 1, j, false) ||
                        horizontalSurround(player, i + 1, j, false) ||
                        verticalSurround(player, i + 1, j, false)) {
                        return true;
                    }
                }
                if (grid.isEmpty(Box(i + 1, j + 1))) {
                    if (diagonalSurround(player, i + 1, j + 1, false) ||
                        horizontalSurround(player, i + 1, j + 1, false) ||
                        verticalSurround(player, i + 1, j + 1, false)) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

bool GameOthello::horizontalSurround(const int player, const int i, const int j, const bool flip) {
    bool isFlip1 = false;
    bool end = false;
    if (isPlayer(player, i, j + 1) || grid.isEmpty(Box(i, j + 1))) {
        end = true;
    }
    for (int y = j + 1; y < grid.getNB_COLUMN() && !isFlip1 && !end; y++) {
        if (grid.getgrid()[i][y] == player) {
            isFlip1 = true;
            if (flip) {
                int k = y;
                for (int y = k; y > j; y--) {
                    grid.getgrid()[i][y] = player;
                }
            }
        }
    }
    bool isFlip2 = false;
    end = false;
    if (isPlayer(player, i, j - 1) || grid.isEmpty(Box(i, j - 1))) {
        end = true;
    }
    for (int y = j - 1; y >= 0 && !isFlip2 && !end; y--) {
        if (grid.getgrid()[i][y] == player) {
            isFlip2 = true;
            if (flip) {
                int k = y;
                for (int y = k; y < j; y++) {
                    grid.getgrid()[i][y] = player;
                }
            }
        }
    }
    return isFlip1 || isFlip2;
}


bool GameOthello::verticalSurround(const int player, const int i, const int j, const bool flip) {
    bool isFlip1 = false;
    bool end = false;
    if (isPlayer(player, i+1, j) || grid.isEmpty(Box(i+1, j))) {
        end = true;
    }
    for (int x = i + 1; x < grid.getNB_LINE() && !isFlip1 && !end; x++) {
        if (grid.getgrid()[x][j] == player) {
            isFlip1 = true;
            if (flip) {
                int k = x;
                for (int x = k; x > i; x--) {
                    grid.getgrid()[x][j] = player;
                }
            }
        }
    }
    bool isFlip2 = false;
    end = false;
    if (isPlayer(player, i - 1, j) || grid.isEmpty(Box(i - 1, j))) {
        end = true;
    }
    for (int x = i - 1; x >= 0 && !isFlip2 && !end; x--) {
        if (grid.getgrid()[x][j] == player) {
            isFlip2 = true;
            if (flip) {
                int k = x;
                for (int x = k; x < i; x++) {
                    grid.getgrid()[x][j] = player;
                }
            }
        }
    }
    return isFlip1 || isFlip2;
}

bool GameOthello::diagonalSurround(const int player, const int i, const int j, const bool flip) {

    bool isFlip1 = false;
    bool fin = false;

    if (isPlayer(player, i + 1, j + 1) || grid.isEmpty(Box(i + 1, j + 1))) {
        fin = true;
    }

    for (int x = i + 1; x < grid.getNB_LINE() && !isFlip1 && !fin; x++) {
        for (int y = j + 1; y < grid.getNB_COLUMN() && !isFlip1 && !fin; y++) {
            if (grid.getgrid()[x][y] == player) {
                isFlip1 = true;
                if (flip) {
                    for (int k = x; k > i; k--) {
                        for (int k2 = y; k2 > j; k2--) {
                            grid.getgrid()[k][k2] = player;
                            k--;
                        }
                    }
                }

            }
            if (grid.isEmpty(Box(x, y))) {
                fin = true;
            }
            x++;
        }
    }

    fin = false;
    bool isFlip2 = false;

    if (isPlayer(player, i - 1, j - 1) || grid.isEmpty(Box(i - 1, j - 1))) {
        fin = true;
    }

    for (int x = i - 1; x >= 0 && !isFlip2 && !fin; x--) {
        for (int y = j - 1; y >= 0 && !isFlip2 && !fin; y--) {
            if (grid.getgrid()[x][y] == player) {
                isFlip2 = true;
                if (flip) {
                    for (int k = x; k < i; k++) {
                        for (int k2 = y; k2 < j; k2++) {
                            grid.getgrid()[k][k2] = player;
                            k++;
                        }
                    }
                }
            }
            if (grid.isEmpty(Box(x, y))) {
                fin = true;
            }
            x--;
        }
    }

    fin = false;
    bool isFlip3 = false;

    if (isPlayer(player, i - 1, j + 1) || grid.isEmpty(Box(i - 1, j + 1))) {
        fin = true;
    }

    for (int x = i - 1; x >= 0 && !isFlip3 && !fin; x--) {
        for (int y = j + 1; y < grid.getNB_COLUMN() && !isFlip3 && !fin; y++) {
            if (grid.getgrid()[x][y] == player) {
                isFlip3 = true;
                if (flip) {
                    for (int k = x; k < i; k++) {
                        for (int k2 = y; k2 > j; k2--) {
                            grid.getgrid()[k][k2] = player;
                            k++;
                        }
                    }
                }
            }
            if (grid.isEmpty(Box(x, y))) {
                fin = true;
            }
            x--;
        }
    }

    fin = false;
    bool isFlip4 = false;

    if (isPlayer(player, i + 1, j - 1) || grid.isEmpty(Box(i + 1, j - 1))) {
        fin = true;
    }

    for (int x = i + 1; x < grid.getNB_LINE()-1 && !isFlip4 && !fin; x++) {
        for (int y = j - 1; y >= 0 && !isFlip4 && !fin; y--) {
            if (grid.getgrid()[x][y] == player) {
                isFlip4 = true;
                if (flip) {
                    for (int k = x; k > i; k--) {
                        for (int k2 = y; k2 < j; k2++) {
                            grid.getgrid()[k][k2] = player;
                            k--;
                        }
                    }
                }
            }
            if (grid.isEmpty(Box(x, y))) {
                fin = true;
            }
            x++;
        }
    }
    return isFlip1 || isFlip2 || isFlip3 || isFlip4;
}

bool GameOthello::putToken(int i, int j, int player) {
    int ennemi;
    if (player == 1) {
        ennemi = 2;
    }
    else {
        ennemi = 1;
    }

    if (!grid.isEmpty(Box(i, j))) {
        return false;
    }

    // verification qu'on pose le jeton à coté d'un ennemi
    if (!(isPlayer(ennemi, i - 1, j - 1) || isPlayer(ennemi, i - 1, j) || isPlayer(ennemi, i - 1, j + 1)
        || isPlayer(ennemi, i, j - 1) || isPlayer(ennemi, i, j + 1) || isPlayer(ennemi, i + 1, j - 1)
        || isPlayer(ennemi, i + 1, j) || isPlayer(ennemi, i + 1, j + 1))) {
        return false;
    }

    bool returnValue = false;
    if (diagonalSurround(player, i, j, true)) {
        grid.getgrid()[i][j] = player;
        returnValue = true;
    }
    if (horizontalSurround(player, i, j, true)) {
        grid.getgrid()[i][j] = player;
        returnValue = true;
    }
    if (verticalSurround(player, i, j, true)) {
        grid.getgrid()[i][j] = player;
        returnValue = true;
    }
    return returnValue;
}

bool GameOthello::isPlayer(const int player, const int i, const int j) {
    if (i>=0 && i<grid.getNB_LINE() && j>=0 && j<grid.getNB_COLUMN())
    {
        if (grid.getgrid()[i][j] == player)
            return true;
    }
    return false;
}

int GameOthello::playerVictory(std::string name1, std::string name2) const {
    int count1 = 0;
    int count2 = 0;
    for (int i = 0; i < grid.getNB_LINE(); i++) {
        for (int j = 0; j < grid.getNB_COLUMN(); j++) {
            if (grid.getgrid()[i][j]==1)
            {
                count1++;
            }
            else if (grid.getgrid()[i][j] == 2) {
                count2++;
            }
        }
    }
    ConsoleOut::getInstance().DisplayScoreOthello(name1, count1);
    ConsoleOut::getInstance().DisplayScoreOthello(name2, count2);
    if (count1>count2)
    {
        return 1;
    }
    return 2;
}


