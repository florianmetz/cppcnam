#include "../Headers/gamecheckers.h"

GameCheckers::GameCheckers(GridCheckers &_grid) : Game(_grid), grid(_grid)
{
}

void GameCheckers::gameOngoing() {
    grid.initCheckers();
    gameStarting();
    grid.displayGrid();

    bool fin = false;
    while (!fin) {
        gameLoop(fin);
    }
}

void GameCheckers::gameLoop(bool &fin){
    bool deplaceJeton = false;
    for (int player = 1; player <= NB_PLAYER && !fin; player++) {
        int count = 0;

        while (!deplaceJeton) {
            deplaceJeton = caseChoose(player);
            if (!deplaceJeton) {
                ConsoleOut::getInstance().ErrorInput(grid.getNB_LINE());
                count++;
                //testMaxError(player, fin, count);
            }
        }

        grid.displayGrid();
        fin = gameEnding();
        deplaceJeton = false;
    }
}


bool GameCheckers::gameEnding() {

        Player p1 = j1;
        std::string name1 = p1.getName();
        Player p2 = j2;
        std::string name2 = p2.getName();

        //congratulations(playerVictory(name1, name2));

    return false;
}

bool GameCheckers::caseChoose(const int player) {
    ConsoleOut::getInstance().CaseSelectTwoInput(player);
    int lineSelected;
    int colSelected;
    ConsoleIn::getInstance().TwoIntInput(lineSelected, colSelected);

    //Si colonne, ligne et case valide
    if (chooseVerification(lineSelected) && chooseVerification(colSelected) && tokenSelectVerification(Box(lineSelected, colSelected), player)) {

        int lineMovedTo;
        int colMovedTo;

        bool fin =false;
        while (!fin){
            ConsoleOut::getInstance().CaseChooseTwoInput(player);
            ConsoleIn::getInstance().TwoIntInput(lineMovedTo, colMovedTo);
            if(chooseVerification(lineMovedTo) && chooseVerification(colMovedTo)&& pawnMoveVerification(Box(lineSelected,colSelected),Box(lineMovedTo, colMovedTo),player)){
                grid.getgrid()[lineSelected][colSelected]=0;
                grid.getgrid()[lineMovedTo][colMovedTo]=player;
                return true;
            }

        }
    }
    return false;
}

//bool GameCheckers::putToken(int lineSelected, int colSelected, Player player) {}

int GameCheckers::playerVictory(std::string name1, std::string name2) const {
    int count1 = 0;
    int count2 = 0;
    for (int i = 0; i < grid.getNB_LINE(); i++) {
        for (int j = 0; j < grid.getNB_COLUMN(); j++) {
            if (grid.getgrid()[i][j]==1)
            {
                count1++;
            }
            else if (grid.getgrid()[i][j] == 2) {
                count2++;
            }
        }
    }
    ConsoleOut::getInstance().DisplayScoreOthello(name1, count1);
    ConsoleOut::getInstance().DisplayScoreOthello(name2, count2);
    if (count1==0)
    {
        return 2;
    }
    else if (count2==0){
        return 1;
    }
    return 0;

}
bool GameCheckers::tokenSelectVerification(Box box, Player player) {

    //Vérifie que ce n'est pas vide
    if (grid.isEmpty(box)) {
        ConsoleOut::getInstance().ErrorBox();
        return false;
    }

    //Vérifie que le jeton est de la couleur du joueur
    if (!isPlayer(player, box)){
        ConsoleOut::getInstance().ErrorColor(player);
        return false;
    }
    return true;
}

bool GameCheckers::pawnMoveVerification(Box boxStart, Box boxEnd,Player player) {

    //int enemy=player.getEnemyId();
    if (boxStart==boxEnd||(grid.isEmpty(boxEnd)&&(grid.isPlayable(boxEnd)))){
        return true;
    }

    //Pas de capture possible
    if(!nextToEnemy(boxStart,player)){
        if (((player.getId()==1 && boxEnd.line!=boxStart.line+1 ) || ( player.getId()==2 && boxEnd.line!=boxStart.line-1))
                ||(boxEnd.column!=boxStart.column+1 && boxEnd.column!=boxStart.column-1 )){
            ConsoleOut::getInstance().ErrorBox();
            return false;
        }
    }
    //Peut capturer et ne capture pas
    else if(nextToEnemy(boxStart,player)&&abs(boxStart-boxEnd)!=2&& !enemyBetween(boxStart,boxEnd,player)){
        ConsoleOut::getInstance().ErrorBox();
        return false;
    }

    //Peut capturer et capture
    else if(nextToEnemy(boxStart,player)&&(abs(boxStart-boxEnd)!=2|| !enemyBetween(boxStart,boxEnd,player))){
        ConsoleOut::getInstance().ErrorBox();
        return false;
    }

    //captureEnemy(box, Player player)

    return true;

}

bool GameCheckers::checkerMoveVerification(Box boxStart, Box boxEnd,Player player) {
    if (boxStart==boxEnd){
        return true;
    }

    if(!grid.isEmpty(boxEnd)){
        std::cout<<"pas vide"<<std::endl;
        return false;
    }

    if (!grid.isPlayable(boxEnd)){
        std::cout<<"pas jouable"<<std::endl;
        return false;
    }

    //Pas de capture possible
    if(!nextToEnemy(boxStart,player)){
        int line=boxStart.line;
        int col=boxStart.column;

        while(line!=boxEnd.line){
            if(grid.getgrid()[line][col]!=0){
                std::cout<<"pion entre"<<std::endl;
                return false;
            }

            if (boxStart.line<boxEnd.line){line++;}else{line--;}
            if (boxStart.column<boxEnd.column){col++;}else{col--;}
        }


        //vérifier que entre départ et arrivé, pas de pion
    }
    //Peut capturer et ne capture pas
    else if(nextToEnemy(boxStart,player)){
        //vérifier plus grande rafle possible
    }

    //Peut capturer et capture
    else if(nextToEnemy(boxStart,player)){
        //vérifier que entre départ et arrivé que des pions ennemis séparés de 2 à chaque fois
    }
    return true;

}

void GameCheckers::captureEnemy(const Box boxEnemy, Player player){
    int line=boxEnemy.line;
    int col=boxEnemy.column;
    grid.getgrid()[line][col]=0;
    //incrément compteurJoueur
}

bool GameCheckers::enemyBetween(const Box boxStart, const Box boxEnd, Player player){
    int lineEnemy=boxStart.line;
    int colEnemy=boxStart.column;
    int enemy=player.getEnemyId();

    if (boxStart.line<boxEnd.line){lineEnemy++;}else{lineEnemy--;}

    if (boxStart.column<boxEnd.column){colEnemy++;}else{colEnemy--;}

    if (grid.getgrid()[lineEnemy][colEnemy]==enemy){
            return true;
    }
    return false;
}


// verification si le joueur est à coté d'un ennemi
bool GameCheckers :: nextToEnemy(Box box, Player player){
    int enemy=player.getEnemyId();
    int line=box.line;
    int col=box.column;
    if (!(isPlayer(enemy, Box (line - 1, col - 1)) || isPlayer(enemy, Box(line - 1, col + 1))
        || isPlayer(enemy, Box(line + 1, col - 1))|| isPlayer(enemy, Box(line + 1, col + 1)))) {
        return false;
    }
    return true;
}

bool GameCheckers::isPlayer(const Player player, Box box) {
    int line=box.line;
    int col=box.column;
    if (line>=0 && line<grid.getNB_LINE() && col>=0 && col<grid.getNB_COLUMN())
    {
        if (grid.getgrid()[line][col] == player.getId())
            return true;
    }
    return false;
}
