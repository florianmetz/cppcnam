#ifndef SAVECHOICE_H
#define SAVECHOICE_H

#include <QDialog>

namespace Ui {
class SaveChoice;
}

class SaveChoice : public QDialog
{
    Q_OBJECT

public:
    explicit SaveChoice(QWidget *parent = nullptr);
    ~SaveChoice();

private:
    Ui::SaveChoice *ui;
};

#endif // SAVECHOICE_H
