#include "../Headers/ConsoleOut.h"

void ConsoleOut::WelcomeMessage(){
    std::cout << "A quoi voulez vous jouer :(1=morpion, 2=puissance4, 3=othello, 4=dames)" << std::endl;
}

void ConsoleOut::ErrorInput(int max){
    std::cout << "Erreur : Il faut rentrer un chiffre entre 0 et "<<max-1<<" sur une case vide et jouable" << std::endl;
}
void ConsoleOut::ErrorColor(const Player player){
    std::cout << "Erreur : Selectionnez un jeton de votre couleur"<<std::endl;
}
void ConsoleOut::ErrorBox(){
    std::cout <<"Erreur: Selectionnez une case valide"<<std::endl;
}
void ConsoleOut::ManualEnd(){
    std::cout << "Si vous pensez que vous ne pouvez plus placer de jeton vous pouvez tapez 1 pour mettre fin à la partie sinon tapez 2" << std::endl;
}
void ConsoleOut::CaseSelectTwoInput(const int player){
    std::cout << "Joueur " << player << ", entre le numero de la colonne du jeton que tu souhaites deplacer :" << std::endl;
}

void ConsoleOut::CaseChooseOneInput(const int player){
    std::cout << "Joueur " << player << ", entre le numero de la colonne ou tu veux mettre ton jeton :" << std::endl;
}

void ConsoleOut::CaseChooseTwoInput(const int player){
    std::cout << "Joueur " << player << ", entre le numero de la ligne et de la colonne ou tu veux mettre ton jeton :" << std::endl;
}

void ConsoleOut::Hello(){
    std::cout << "Bonjour et bienvenue." << std::endl;
}

void ConsoleOut::ChooseName(const int player){
    std::cout << "Joueur " << player << " : Entre ton prenom " << std::endl;
}

void ConsoleOut::StartMessage(){
    std::cout << "C'EST PARTI ! " << std::endl;
}

void ConsoleOut::VictoryMessage(const std::string name){
    std::cout << "Nous avons un VAINQUEUR ! Bravo " << name << " !!!" << std::endl;
}

void ConsoleOut::GenericDraw(){
    std::cout << "La grille est rempli...match nul, vous voulez rejouer ? Y/N" << std::endl;
}

void ConsoleOut::Replay(){
    std::cout << "Allez on joue alors ! " << std::endl;
}

void ConsoleOut::EndGame(){
    std::cout << "Merci d'avoir joué, à la prochaine" << std::endl;
}

void ConsoleOut::DisplayScoreOthello(const std::string name, const int score){
    std::cout << "Score de " << name << " : " << score << std::endl;
}
