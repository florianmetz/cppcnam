#include "../Headers/ConsoleIn.h"

std::string ConsoleIn::StringInput(){
    std::string input;
    std::cin >> input;
    std::cin.clear();
    return input;
}

void ConsoleIn::TwoIntInput(int &first, int &second){
    std::string ligne;
    std::string colonne;
    std::cin >> ligne >> colonne;
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    first = std::stoi(ligne);
    second = std::stoi(colonne);
}

int ConsoleIn::IntInput(){
    std::string input;
    std::cin >> input;
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return std::stoi(input);
}
