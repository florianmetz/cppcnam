QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Game/gamecheckers.cpp \
        Game/gamemorpion.cpp \
        Game/gameothello.cpp \
        Game/gamepower4.cpp \
        Game/game.cpp \
    Grid/box.cpp \
        Grid/grid.cpp \
    Grid/gridcheckers.cpp \
        Grid/gridmorpion.cpp \
        Grid/gridothello.cpp \
        Grid/gridpower4.cpp \
        Player/player.cpp \
        Save/save.cpp \
    Token/token.cpp \
    Views/ConsoleIn.cpp \
    Views/ConsoleOut.cpp \
        igameothello.cpp \
        main.cpp \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    Grid/gridcheckers.h \
    Headers/ConsoleIn.h \
    Headers/ConsoleOut.h \
    Headers/box.h \
    Headers/game.h \
    Headers/gamecheckers.h \
    Headers/gamemorpion.h \
    Headers/gameothello.h \
    Headers/gamepower4.h \
    Headers/grid.h \
    Headers/gridcheckers.h \
    Headers/gridmorpion.h \
    Headers/gridothello.h \
    Headers/gridpower4.h \
    Headers/igameothello.h \
    Headers/player.h \
    Headers/save.h \
    Headers/token.h \
    gamecheckers.h

DISTFILES +=
