#include "Headers/gamemorpion.h"
#include "Headers/gamepower4.h"
#include "Headers/gameothello.h"
#include "Headers/gamecheckers.h"
#include "Headers/ConsoleOut.h"
#include "Headers/ConsoleIn.h"

//#include "Headers/save.h"
int main(int argc, char* argv[])
{

    /*Grid g(3,3,3);
    g.displayGrid();
    g.getgrid()[0][0]=2;
    g.getgrid()[1][1]=2;
    g.getgrid()[0][1]=1;
    g.getgrid()[1][0]=1;
    g.getgrid()[2][2]=1;
    Save s = Save(g);
    s.ecriture();
    Grid g2(3,3,3);
    Save s2 = Save(g2);
    s2.lecture();
    g2.displayGrid();*/
    //return 0;

    bool end = false;
    while (!end) {
        ConsoleOut::getInstance().WelcomeMessage();
        std::string game = ConsoleIn::getInstance().StringInput();
        if (game == "1") {
            GridMorpion grid = GridMorpion();
            GameMorpion game = GameMorpion((GridMorpion&)grid);
            game.gameOngoing();
            end = true;
        }
        if (game == "2") {
            GridPower4 grid = GridPower4();
            GamePower4 game = GamePower4((GridPower4&)grid);
            game.gameOngoing();
            end = true;
        }
        if (game == "3") {
            GridOthello grid = GridOthello();
            GameOthello game = GameOthello((GridOthello&)grid);
            game.gameOngoing();
            end = true;
        }
        if (game == "4") {
            GridCheckers grid = GridCheckers();
            GameCheckers game = GameCheckers((GridCheckers&)grid);
            game.gameOngoing();
            end = true;
        }
    }
}
