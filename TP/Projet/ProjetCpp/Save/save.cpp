#include "../Headers/save.h"

Save::Save(Grid &_grid) :grid(_grid)
{
}



void Save::lecture()
{
    QFile file(FILENAME);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        cerr << "ERREUR: Impossible de lire le fichier" << endl;
        return;
    }

    QTextStream in(&file);
    int i=0;
    while(!in.atEnd()){
        QString line = in.readLine();
        for (int j=0; j<line.length(); j++){
            this->grid.getgrid()[i][j]= line.toStdString()[j];
        }
        i++;
    }
}

void Save::ecriture()
{
    QFile file(FILENAME);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)){
        cerr << "ERREUR: Impossible d'ecrire dans le fichier." << endl;
        return;
    }
    QTextStream out(&file);
    for (int i=0; i<this->grid.getNB_LINE(); i++){
        for (int j=0; j<this->grid.getNB_LINE(); j++){
            out << this->grid.getgrid()[i][j];
        }
        out<< "\n";
    }
    file.close();

}
