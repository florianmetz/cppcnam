#include "manipulationNombre.h"

int main (int argc, char const *argv[]){
    std::cout << "_______________________Q1.1_______________________\n";
    std::cout << "2 + 3 = " << AdditionInteger(2,3) << "\n";
    std::cout << "_______________________Q1.2_______________________\n";
    SwitchNumber(2,3);
    std::cout << "_______________________Q1.3_______________________\n";
    ReplaceThirdNumberPointer(2,3,10);
    std::cout << "_____________________Q1.3 bis_____________________\n";
    ReplaceThirdNumberReference(2,3,10);
    std::cout << "_______________________Q1.4_______________________\n";
    GenerateDisplayAndSortTab();
    return 0;
}

int AdditionInteger(int a, int b){
    return a+b;
}

void SwitchNumber(int a, int b){
    std::cout << "a vaut " << a << " et b vaut " << b << "\n";
    int temp=a;
    a=b;
    b=temp;
    std::cout << "a vaut desormais " << a << " et b vaut desormais " << b << "\n";
}

void ReplaceThirdNumberPointer(int a, int b, int c){
    int somme = AdditionInteger(a,b);
    std::cout<< "a = " << a << " b = "<< b << "c = " << c << "\n";
    int* temp = &somme ;
    c=*temp ;
    std::cout<< "c = " << c << "\n";
}

void ReplaceThirdNumberReference(int a, int b, int c){
    int& temp=c;
    std::cout<< "a = " << a << " b = "<< b << "c = " << c << "\n";
    int somme = AdditionInteger(a,b);
    c = somme;
    std::cout<< "c = " << temp << "\n";
}

void GenerateDisplayAndSortTab(){
    int const size= 10;
    int tab[size];
    for (int i=0 ; i< size;i++ ){
        tab[i] = rand()%10 +1; //nombres entre 1 et 10
        std::cout << tab[i] << "\t";
    }

    std::cout << "\nSorting ... \n";

    for(int i = size-1; i>=1 ; i--){
        for (int j =0; j<=i-1; j++){
            if(tab[j+1]<tab[j]){
                int temp = tab[j+1];
                tab[j+1]=tab[j];
                tab[j]=temp;
            }
        }
    }

    std::cout << "Sorted tab : \n";
    for (int i=0 ; i< size;i++ ){
        std::cout << tab[i] << "\t";
    }
    
}