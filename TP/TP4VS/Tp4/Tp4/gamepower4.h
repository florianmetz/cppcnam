#ifndef GAMEPOWER4_H
#define GAMEPOWER4_H

#include "game.h"

class GamePower4 : public Game
{
public:
	GamePower4(GridPower4* _grid);
	void gameOngoing() override;
	bool caseChoose(const int player) override;
	bool gameEnding() override;

private:
	GridPower4* grid;
};
#endif // GAMEPOWER4_H
