#ifndef GAMEMORPION_H
#define GAMEMORPION_H

#include "game.h"

class GameMorpion : public Game
{
public:
	GameMorpion(GridMorpion* _grid);
	void gameOngoing() override;
	bool caseChoose(const int player) override;
	bool gameEnding() override;

private:
	GridMorpion* grid;
};

#endif // GAMEMORPION_H