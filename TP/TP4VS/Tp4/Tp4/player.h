#ifndef PLAYER_H
#define PLAYER_H

#include <string>

class Player
{
public:
    Player(int id);

    int getId() const
    {
        return id;
    }

    const std::string getName() const
    {
        return name;
    }

    void setNom(const std::string& newName)
    {
        name = newName;
    }


private:
    int id;
    std::string name;
};

#endif // PLAYER_H
