#ifndef GRIDMORPION_H
#define GRIDMORPION_H

#include "IGridMorpion.h"

class GridMorpion : public IGridMorpion
{
public:
    GridMorpion();
    bool putToken(int i, int j, int player) override;
    bool completeDiagonal(int diagonal, int player) const override; //j etant le numero de colonne d'un coin du tableau
    bool playerVictory(int player) const override;
};

#endif // GRIDMORPION_H
