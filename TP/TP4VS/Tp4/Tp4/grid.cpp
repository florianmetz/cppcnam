#include "grid.h"

Grid::Grid(const int nbLine, const int nbColumn, const int nbTokenForVictory) :NB_LINE(nbLine), NB_COLUMN(nbColumn), NB_TOKEN_FOR_VICTORY(nbTokenForVictory)
{
}

void Grid::init() {
    grid.assign(NB_LINE, std::vector<int>(NB_COLUMN, 0));
}

bool Grid::isEmpty(const int i, const int j) const
{
    if (i >= 0 && i < NB_LINE && j >= 0 && j < NB_COLUMN)
    {
        if (this->grid[i][j] == 0)
            return true;
    }
    return false;
}

bool Grid::isFull()const {
    for (int i = 0; i < NB_LINE; i++) {
        for (int j = 0; j < NB_COLUMN; j++) {
            if (this->grid[i][j] == 0) {
                return false;
            }
        }
    }
    return true;
}

bool Grid::completeLine(const int i, const int player) const
{
    int count = 0;
    for (int j = 0; j < NB_LINE; j++) {
        if (this->grid[i][j] != player) {
            return false;
        }
        count++;
        if (count == NB_TOKEN_FOR_VICTORY) {
            return true;
        }
    }
    return true;
}

bool Grid::completeColumn(const int j, const int player) const
{
    int count = 0;
    for (int i = 0; i < NB_COLUMN; i++) {
        if (this->grid[i][j] != player) {
            return false;
        }
        count++;
        if (count == NB_TOKEN_FOR_VICTORY) {
            return true;
        }
    }
    return true;
}

void Grid::displayGrid() const
{
    std::cout << "\\ ";
    for (int j = 0; j < NB_COLUMN;j++) {
        std::cout << j << " ";
    }
    std::cout << std::endl;
    for (int i = 0; i < NB_LINE; i++) {
        std::cout << i << " ";
        for (int j = 0; j < NB_COLUMN; j++) {
            if (this->grid[i][j] == 0) {
                std::cout << "_ ";
            }
            else {
                std::cout << this->grid[i][j] << " ";
            }
        }
        std::cout << std::endl;
    }
}


