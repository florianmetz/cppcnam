#include "gamepower4.h"

GamePower4::GamePower4(GridPower4* _grid) : Game(_grid), grid(_grid)
{
    gameOngoing();
}

void GamePower4::gameOngoing() {
    grid->init();
    gameStarting();
    grid->displayGrid();

    bool fin = false;
    bool deposeJeton = false;
    while (!fin) {
        for (int i = 1; i < 3 && !fin; i++) {
            while (!deposeJeton) {
                deposeJeton = caseChoose(i);
                if (!deposeJeton) {
                    std::cout << "Error : Il faut rentrer un chiffre entre 0 et 7 sur une case vide" << std::endl;
                }
            }
            grid->displayGrid();
            fin = this->gameEnding();
            deposeJeton = false;
        }
    }
}


bool GamePower4::caseChoose(const int player) {
    GridPower4* gridPower4 = static_cast<GridPower4*>(this->grid);
    std::cout << "Joueur " << player << ", entre le numero de la colonne ou tu veux mettre ton jeton :" << std::endl;
    std::string colonne;
    std::cin >> colonne;

    int c = std::stoi(colonne);

    if (chooseVerification(c) && gridPower4->putToken(c, player)) {
        return true;
    }
    return false;
}

bool GamePower4::gameEnding() {
    GridPower4* gridPower4 = static_cast<GridPower4*>(this->grid);
    //si quelqu'un a gagn�
    if (gridPower4->playerVictory(1)) {
        congratulations(1);
        return true;
    }
    else if (gridPower4->playerVictory(2)) {
        congratulations(2);
        return true;
    }
    //si la grid est pleine
    else if (grid->isFull()) {
        draw();
        return true;
    }
    else {
        return false;
    }
}
