#include "gamemorpion.h"

GameMorpion::GameMorpion(GridMorpion* _grid) : Game(_grid), grid(_grid)
{
    gameOngoing();
}

void GameMorpion::gameOngoing() {
    grid->init();
    gameStarting();
    grid->displayGrid();
    
    bool fin = false;
    bool deposeJeton = false;
    while (!fin) {
        for (int i = 1; i < 3 && !fin; i++) {
            while (!deposeJeton) {
                deposeJeton = caseChoose(i);
                if (!deposeJeton) {
                    std::cout << "Error : Il faut rentrer un chiffre entre 0 et 2 sur une case vide" << std::endl;
                }
            }
            grid->displayGrid();
            fin = this->gameEnding();
            deposeJeton = false;
        }
    }
}

bool GameMorpion::caseChoose(const int player) {
    GridMorpion* gridMorpion = static_cast<GridMorpion*>(this->grid);
    std::cout << "Joueur " << player << ", entre le numero de la ligne et de la colonne ou tu veux mettre ton jeton :" << std::endl;
    std::string ligne;
    std::string colonne;
    std::cin >> ligne >> colonne;

    int l = std::stoi(ligne);
    int c = std::stoi(colonne);

    if (chooseVerification(l) && chooseVerification(c) && gridMorpion->putToken(l, c, player)) {
        return true;
    }
    return false;
}

bool GameMorpion::gameEnding() {
    GridMorpion* gridMorpion = static_cast<GridMorpion*>(this->grid);
    //si quelqu'un a gagn�
    if (gridMorpion->playerVictory(1)) {
        congratulations(1);
        return true;
    }
    else if (gridMorpion->playerVictory(2)) {
        congratulations(2);
        return true;
    }
    //si la grid est pleine
    else if (grid->isFull()) {
        draw();
        return true;
    }
    else {
        return false;
    }
}

