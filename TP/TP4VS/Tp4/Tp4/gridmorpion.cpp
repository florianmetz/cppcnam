#include "gridmorpion.h"


GridMorpion::GridMorpion() : IGridMorpion()
{
    init();
}

bool GridMorpion::putToken(int i, int j, int player)
{
    if (this->isEmpty(i, j)) {
        this->grid[i][j] = player;
        return true;
    }
    return false;
}

bool GridMorpion::completeDiagonal(int diagonal, int player) const
{
    if (diagonal == 0) {
        for (int i = 0; i < 3; i++) {
            if (!(grid[i][i] == player))
                return false;
        }
    }
    else if (diagonal == 2) {
        int j = 2;
        for (int i = 0; i < 3; i++) {
            if (!(grid[i][j] == player))
                return false;
            j--;
        }
    }
    else {
        return false;
    }
    return true;

}

bool GridMorpion::playerVictory(int player) const
{
    for (int i = 0; i < 3; i++) {
        if (completeColumn(i, player) || completeLine(i, player)) {
            return true;
        }
    }

    return (completeDiagonal(0, player) || completeDiagonal(2, player));
}
