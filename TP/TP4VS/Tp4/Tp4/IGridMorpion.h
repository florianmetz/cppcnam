#ifndef IGRIDMORPION_H
#define IGRIDMORPION_H

#include "grid.h"

class IGridMorpion : public Grid
{
public:
    IGridMorpion();
    virtual bool putToken(int i, int j, int player) = 0;
    virtual bool completeDiagonal(int diagonal, int player) const = 0; //j etant le numero de colonne d'un coin du tableau
    virtual bool playerVictory(int player) const = 0;
};

#endif // IGRIDMORPION_H
