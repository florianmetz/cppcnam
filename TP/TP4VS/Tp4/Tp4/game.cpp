#include "game.h"

Game::Game(Grid* _grid) :grid(_grid)
{
}

void Game::gameStarting() {
    std::cout << "Bonjour et bienvenue." << std::endl;

    for (int i = 1; i < 3; i++) {
        std::cout << "Joueur " << i << " : Entre ton prenom " << std::endl;
        std::string prenom;
        std::cin >> prenom;
        if (i == 1)
            j1.setNom(prenom);
        else
            j2.setNom(prenom);
    }
    std::cout << "C'EST PARTI ! " << std::endl;
}

bool Game::chooseVerification(const int i) const
{
    if (i<0 || i>this->grid->getNB_COLUMN()) {
        return false;
    }
    return true;
}

void Game::congratulations(const int winner) const
{
    if (winner == 1) {
        std::cout << "Nous avons un VAINQUEUR ! Bravo " << j1.getName() << " !!!" << std::endl;
    }
    else {
        std::cout << "Nous avons un VAINQUEUR ! Bravo " << j2.getName() << " !!!" << std::endl;
    }
}

void Game::draw()
{
    std::cout << "La grid est rempli...match nul, vous voulez rejouer ? Y/N" << std::endl;
    std::string saisie;
    std::cin >> saisie;

    if (saisie == "Y") {
        std::cout << "Allez on joue alors ! " << std::endl;
    }
    else {
        std::cout << "Merci d'avoir jou�, � la prochaine" << std::endl;
    }
}

bool Game::gameEnding() {
    return false;
}

