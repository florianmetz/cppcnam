#ifndef IGRIDPOWER4_H
#define IGRIDPOWER4_H

#include "grid.h"

class IGridPower4 : public Grid
{
public:
    IGridPower4();
    virtual bool putToken(int j, int player) = 0;  //j numero de colonne dans laquelle on veut mettre un jeton
    virtual bool completeDiagonal(int player) const = 0;
    virtual bool playerVictory(int player) const = 0;
};

#endif // IGRIDPOWER4_H
