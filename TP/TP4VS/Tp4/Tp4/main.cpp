#include "gamemorpion.h"
#include "gamepower4.h"
#include "gameothello.h"
int main(int argc, char* argv[])
{
    std::string game;
    bool end = false;
    while (!end) {
        std::cout << "A quoi voulez vous jouer :(1=morpion, 2=puissance4, 3=othello)" << std::endl;
        std::cin >> game;
        if (game == "1") {
            GameMorpion(new GridMorpion);
            end = true;
        }
        if (game == "2") {
            GamePower4(new GridPower4);
            end = true;
        }
        if (game == "3") {
            GameOthello(new GridOthello);
        }
    }
}
