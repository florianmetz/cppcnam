#ifndef GRIDPOWER4_H
#define GRIDPOWER4_H

#include "IGridPower4.h"

class GridPower4 :public IGridPower4
{
public:
    GridPower4();
    bool putToken(int j, int player) override;  //j numero de colonne dans laquelle on veut mettre un jeton
    bool completeDiagonal(int player) const override;
    bool playerVictory(int player) const override;
};

#endif // GRIDPOWER4_H
