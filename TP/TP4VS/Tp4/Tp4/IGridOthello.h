#ifndef IGRIDOTHELLO_H
#define IGRIDOTHELLO_H

#include "grid.h"

class IGridOthello : public Grid
{
public:
	IGridOthello();
	virtual bool putToken(int i, int j, int player) = 0;
	virtual int playerVictory(std::string name1, std::string name2) const = 0;
	virtual void initOthello() = 0;
	virtual bool placeable(const int player) = 0;
	virtual bool horizontalSurround(const int player, const int i, const int j, const bool flip) = 0;
	virtual bool verticalSurround(const int player, const int i, const int j, const bool flip) = 0;
	virtual bool diagonalSurround(const int player, const int i, const int j, const bool flip) = 0;
	virtual bool isPlayer(const int player, const int i, const int j) = 0;
};

#endif // IGRIDOTHELLO_H
