#ifndef GAMEOTHELLO_H
#define GAMEOTHELLO_H

#include "IGameOthello.h"

class GameOthello : public IGameOthello
{
public:
	GameOthello(GridOthello* _grid);
	void gameOngoing() override;
	bool caseChoose(const int player) override;
	bool gameEnding(const int player) override;

private:
	GridOthello* grid;
};

#endif // GAMEOTHELLO_H