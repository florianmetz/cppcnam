#include "gameothello.h"

GameOthello::GameOthello(GridOthello* _grid) : IGameOthello(_grid), grid(_grid)
{
    gameOngoing();
}

void GameOthello::gameOngoing() {
    grid->initOthello();
    gameStarting();
    grid->displayGrid();

    bool fin = false;
    bool deposeJeton = false;
    while (!fin) {
        for (int player = 1; player < 3 && !fin; player++) {
            int count = 0;
            while (!deposeJeton) {
                deposeJeton = caseChoose(player);

                if (!deposeJeton) {
                    std::cout << "Error : Il faut rentrer un chiffre entre 0 et 7 une des cases possibles" << std::endl;
                    count++;
                    if (count > 3)
                    {
                        std::cout << "Si vous pensez que vous ne pouvez plus placer de jeton vous pouvez tapez 1 pour mettre fin � la partie sinon tapez 2" << std::endl;
                        std::string choice;
                        std::cin >> choice;
                        int var = std::stoi(choice);
                        if (var == 1)
                        {
                           fin = this->gameEnding(player);
                        }
                    }
                }
            }

            grid->displayGrid();
            fin = this->gameEnding(0);
            deposeJeton = false;
        }
    }
}

bool GameOthello::caseChoose(const int player) {
    GridOthello* gridOthello = static_cast<GridOthello*>(this->grid);
    std::cout << "Joueur " << player << ", entre le numero de la ligne et de la colonne ou tu veux mettre ton jeton :" << std::endl;
    std::string ligne;
    std::string colonne;
    std::cin >> ligne >> colonne;

    int l = std::stoi(ligne);
    int c = std::stoi(colonne);

    if (chooseVerification(l) && chooseVerification(c) && gridOthello->putToken(l, c, player)) {
        return true;
    }
    return false;
}

bool GameOthello::gameEnding(const int player) {
    GridOthello* gridOthello = static_cast<GridOthello*>(this->grid);
    //si la grid est pleine
    if (grid->isFull() || player!=0) {
        Player p1 = this->j1;
        std::string name1 = p1.getName();
        Player p2 = this->j2;
        std::string name2 = p2.getName();
        congratulations(gridOthello->playerVictory(name1, name2));

        return true;
    }
}