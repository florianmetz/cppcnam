#include "gridothello.h"

GridOthello::GridOthello() : IGridOthello() 
{
    initOthello();
}

void GridOthello::initOthello() {
    init();
	grid[3][3] = 1;
	grid[4][4] = 1;
	grid[3][4] = 2;
	grid[4][3] = 2;
}

bool GridOthello::placeable(const int player){
    int ennemi;
    if (player == 1) {
        ennemi = 2;
    }
    else {
        ennemi = 1;
    }

    for (int i = 0; i < NB_LINE; i++) {
        for (int j = 0; j < NB_COLUMN; j++) {
            if (grid[i][j] == ennemi) {
                //verification qu'il y a une place autour de l'ennemi
                if (this->isEmpty(i - 1, j - 1)) {
                    if (diagonalSurround(player, i - 1, j - 1, false) ||
                        horizontalSurround(player, i - 1, j - 1, false) ||
                        verticalSurround(player, i - 1, j - 1, false)) {
                        return true;
                    }
                }
                if (this->isEmpty(i - 1, j)) {
                    if (diagonalSurround(player, i - 1, j, false) ||
                        horizontalSurround(player, i - 1, j, false) ||
                        verticalSurround(player, i - 1, j, false)) {
                        return true;
                    }
                }
                if (this->isEmpty(i - 1, j + 1)) {
                    if (diagonalSurround(player, i - 1, j + 1, false) ||
                        horizontalSurround(player, i - 1, j + 1, false) ||
                        verticalSurround(player, i - 1, j + 1, false)) {
                        return true;
                    }
                }
                if (this->isEmpty(i, j - 1)) {
                    if (diagonalSurround(player, i, j - 1, false) ||
                        horizontalSurround(player, i, j - 1, false) ||
                        verticalSurround(player, i, j - 1, false)) {
                        return true;
                    }
                }
                if (this->isEmpty(i, j + 1)) {
                    if (diagonalSurround(player, i, j + 1, false) ||
                        horizontalSurround(player, i, j + 1, false) ||
                        verticalSurround(player, i, j + 1, false)) {
                        return true;
                    }
                }
                if (this->isEmpty(i + 1, j - 1)) {
                    if (diagonalSurround(player, i + 1, j - 1, false) ||
                        horizontalSurround(player, i + 1, j - 1, false) ||
                        verticalSurround(player, i + 1, j - 1, false)) {
                        return true;
                    }
                }
                if (this->isEmpty(i + 1, j)) {
                    if (diagonalSurround(player, i + 1, j, false) ||
                        horizontalSurround(player, i + 1, j, false) ||
                        verticalSurround(player, i + 1, j, false)) {
                        return true;
                    }
                }
                if (this->isEmpty(i + 1, j + 1)) {
                    if (diagonalSurround(player, i + 1, j + 1, false) ||
                        horizontalSurround(player, i + 1, j + 1, false) ||
                        verticalSurround(player, i + 1, j + 1, false)) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

bool GridOthello::horizontalSurround(const int player, const int i, const int j, const bool flip) {
    bool isFlip1 = false;
    bool end = false;
    if (this->isPlayer(player, i, j + 1) || this->isEmpty(i, j + 1)) {
        end = true;
    }
    for (int y = j + 1; y < NB_COLUMN && !isFlip1 && !end; y++) {
        if (this->grid[i][y] == player) {
            isFlip1 = true;
            if (flip) {
                int k = y;
                for (int y = k; y > j; y--) {
                    grid[i][y] = player;
                }
            }
        }
    }
    bool isFlip2 = false;
    end = false;
    if (this->isPlayer(player, i, j - 1) || this->isEmpty(i, j - 1)) {
        end = true;
    }
    for (int y = j - 1; y >= 0 && !isFlip2 && !end; y--) {
        if (this->grid[i][y] == player) {
            isFlip2 = true;
            if (flip) {
                int k = y;
                for (int y = k; y < j; y++) {
                    grid[i][y] = player;
                }
            }
        }
    }
    return isFlip1 || isFlip2;
}


bool GridOthello::verticalSurround(const int player, const int i, const int j, const bool flip) {
    bool isFlip1 = false;
    bool end = false;
    if (this->isPlayer(player, i+1, j) || this->isEmpty(i+1, j)) {
        end = true;
    }
    for (int x = i + 1; x < NB_LINE && !isFlip1 && !end; x++) {
        if (this->grid[x][j] == player) {
            isFlip1 = true;
            if (flip) {
                int k = x;
                for (int x = k; x > i; x--) {
                    grid[x][j] = player;
                }
            }
        }
    }
    bool isFlip2 = false;
    end = false;
    if (this->isPlayer(player, i - 1, j) || this->isEmpty(i - 1, j)) {
        end = true;
    }
    for (int x = i - 1; x >= 0 && !isFlip2 && !end; x--) {
        if (this->grid[x][j] == player) {
            isFlip2 = true;
            if (flip) {
                int k = x;
                for (int x = k; x < i; x++) {
                    grid[x][j] = player;
                }
            }
        }
    }
    return isFlip1 || isFlip2;
}

bool GridOthello::diagonalSurround(const int player, const int i, const int j, const bool flip) {

    bool isFlip1 = false;
    bool fin = false;

    if (this->isPlayer(player, i + 1, j + 1) || this->isEmpty(i + 1, j + 1)) {
        fin = true;
    }

    for (int x = i + 1; x < NB_LINE && !isFlip1 && !fin; x++) {
        for (int y = j + 1; y < NB_COLUMN && !isFlip1 && !fin; y++) {
            if (this->grid[x][y] == player) {
                isFlip1 = true;
                if (flip) {
                    for (int k = x; k > i; k--) {
                        for (int k2 = y; k2 > j; k2--) {
                            grid[k][k2] = player;
                            k--;
                        }
                    }
                }

            }
            if (this->isEmpty(x, y)) {
                fin = true;
            }
            x++;
        }
    }

    fin = false;
    bool isFlip2 = false;

    if (this->isPlayer(player, i - 1, j - 1) || this->isEmpty(i - 1, j - 1)) {
        fin = true;
    }

    for (int x = i - 1; x >= 0 && !isFlip2 && !fin; x--) {
        for (int y = j - 1; y >= 0 && !isFlip2 && !fin; y--) {
            if (this->grid[x][y] == player) {
                isFlip2 = true;
                if (flip) {
                    for (int k = x; k < i; k++) {
                        for (int k2 = y; k2 < j; k2++) {
                            grid[k][k2] = player;
                            k++;
                        }
                    }
                }
            }
            if (this->isEmpty(x, y)) {
                fin = true;
            }
            x--;
        }
    }

    fin = false;
    bool isFlip3 = false;

    if (this->isPlayer(player, i - 1, j + 1) || this->isEmpty(i - 1, j + 1)) {
        fin = true;
    }

    for (int x = i - 1; x >= 0 && !isFlip3 && !fin; x--) {
        for (int y = j + 1; y < NB_COLUMN && !isFlip3 && !fin; y++) {
            if (this->grid[x][y] == player) {
                isFlip3 = true;
                if (flip) {
                    for (int k = x; k < i; k++) {
                        for (int k2 = y; k2 > j; k2--) {
                            grid[k][k2] = player;
                            k++;
                        }
                    }
                }
            }
            if (this->isEmpty(x, y)) {
                fin = true;
            }
            x--;
        }
    }

    fin = false;
    bool isFlip4 = false;

    if (this->isPlayer(player, i + 1, j - 1) || this->isEmpty(i + 1, j - 1)) {
        fin = true;
    }

    for (int x = i + 1; x < NB_LINE-1 && !isFlip4 && !fin; x++) {
        for (int y = j - 1; y >= 0 && !isFlip4 && !fin; y--) {
            if (this->grid[x][y] == player) {
                isFlip4 = true;
                if (flip) {
                    for (int k = x; k > i; k--) {
                        for (int k2 = y; k2 < j; k2++) {
                            grid[k][k2] = player;
                            k--;
                        }
                    }
                }
            }
            if (this->isEmpty(x, y)) {
                fin = true;
            }
            x++;
        }
    }
    return isFlip1 || isFlip2 || isFlip3 || isFlip4;
}

bool GridOthello::putToken(int i, int j, int player) {
    int ennemi;
    if (player == 1) {
        ennemi = 2;
    }
    else {
        ennemi = 1;
    }

    if (!this->isEmpty(i, j)) {
        return false;
    }

    // verification qu'on pose le jeton � cot� d'un ennemi
    if (!(this->isPlayer(ennemi, i - 1, j - 1) || this->isPlayer(ennemi, i - 1, j) || this->isPlayer(ennemi, i - 1, j + 1)
        || this->isPlayer(ennemi, i, j - 1) || this->isPlayer(ennemi, i, j + 1) || this->isPlayer(ennemi, i + 1, j - 1)
        || this->isPlayer(ennemi, i + 1, j) || this->isPlayer(ennemi, i + 1, j + 1))) {
        return false;
    }

    bool returnValue = false;
    if (diagonalSurround(player, i, j, true)) {
        this->grid[i][j] = player;
        returnValue = true;
    }
    if (horizontalSurround(player, i, j, true)) {
        this->grid[i][j] = player;
        returnValue = true;
    }
    if (verticalSurround(player, i, j, true)) {
        this->grid[i][j] = player;
        returnValue = true;
    }
    return returnValue;
}

bool GridOthello::isPlayer(const int player, const int i, const int j) {
    if (i>=0 && i<7 && j>=0 && j<7)
    {
        if (this->grid[i][j] == player)
            return true;
    }
    return false;
}

int GridOthello::playerVictory(std::string name1, std::string name2) const {
    int count1 = 0;
    int count2 = 0;
    for (int i = 0; i < NB_LINE; i++) {
        for (int j = 0; j < NB_COLUMN; j++) {
            if (this->grid[i][j]==1)
            {
                count1++;
            }
            else if (this->grid[i][j] == 2) {
                count2++;
            }
        }
    }
    std::cout << "Score de " << name1 << " : " << count1 << std::endl;
    std::cout << "Score de " << name2 << " : " << count2 << std::endl;
    if (count1>count2)
    {
        return 1;
    }
    return 2;
}


