#ifndef GRIDOTHELLO_H
#define GRIDOTHELLO_H

#include "IGridOthello.h"

class GridOthello : public IGridOthello
{
public:
	GridOthello();
	bool putToken(int i, int j, int player) override;
	int playerVictory(std::string name1, std::string name2) const override;
	void initOthello() override;
	bool placeable(const int player);
	bool horizontalSurround(const int player, const int i, const int j, const bool flip) override;
	bool verticalSurround(const int player, const int i, const int j, const bool flip) override;
	bool diagonalSurround(const int player, const int i, const int j, const bool flip) override;
	bool isPlayer(const int player, const int i, const int j) override;
};

#endif // GRIDOTHELLO_H
