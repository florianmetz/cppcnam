#include "point.h"

Point::Point(float x, float y):x(x),y(y){

}

float Point::calculDistance(Point point1){
    return sqrt(pow(point1.x-x,2)+pow(point1.y-y,2));
}

void Point::Afficher() const{
    std::cout << "x = "<< this->x << " y = " << this->y <<"\n"
                 "La classe Point a une methode calculDistance(Point point)\n"
                 "Cette methode permet de calculer la distance entre deux points\n";
}
