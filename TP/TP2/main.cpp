#include "point.h"
#include "cercle.h"
#include"rectangle.h"
#include "triangle.h"
int main(int argc, char *argv[])
{
    //Point test
    Point point = Point(0,0);
    Point point1 =Point(0,2);
    std::cout << "Distance entre les deux point : " << point.calculDistance(point1) << "\n";
    point.Afficher();
    std::cout << "\n";
    //Cercle test
    Cercle cercle = Cercle(4,point);
    std::cout << "Le point est sur le cerlce : " << cercle.isItIn(point) << "(1=true, 0=false)\n";
    std::cout << "Le point est sur le bord cerlce : " << cercle.isItOnBorder(point1) << "(1=true, 0=false)\n";
    cercle.Afficher();
    std::cout << "\n";
    //Rectangle test
    Rectangle rect = Rectangle(6,2,point);
    Rectangle smallerRect = Rectangle(4,2,point);
    std::cout << "smallerRect a une plus petite surface : " << rect.hasBiggerArea(smallerRect) << "(1=true, 0=false)\n";
    std::cout << "smallerRect a un plus petit perimetre " << rect.hasBiggerPerimeter(smallerRect) << "(1=true, 0=false)\n";
    rect.Afficher();
    std::cout << "\n";
    //Triangle test
    Triangle triangleIsoRect=Triangle(Point(1,1), Point(4,4), Point(7,1));
    Triangle equi=Triangle(Point(0,0), Point(0,0), Point(0,0));
    std::cout << "Le triangle est isocele : " << triangleIsoRect.isIsocel() << "(1=true, 0=false)\n";
    std::cout << "Le triangle est rectangle : " << triangleIsoRect.isRectangle() << "(1=true, 0=false)\n"; //return false au lieu de true (c'est probablement un problème d'arrondi)
    std::cout << "Le triangle est equilatéral : " << equi.isEquilateral() << "(1=true, 0=false)\n";
    triangleIsoRect.Afficher();

}
