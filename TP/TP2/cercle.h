#ifndef CERCLE_H
#define CERCLE_H

#include <cmath>
#include <stdlib.h>
#include "point.h"
#define _USE_MATH_DEFINES

class Cercle{

public:
    //Constructeur
    Cercle(int diameter, Point center);

    //getters & setters
    Point getCenter() const{
        return center;
    }
    int getDiameter() const{
        return diameter;
    }

    void setCenter(Point _center){
        center = _center;
    }
    void setDiameter(int _diameter){
        diameter = _diameter;
    }

    //method
    float getPerimeter() const;
    float getArea() const;
    float calculDistanceFromCenter(Point point) const;
    bool isItOnBorder(Point point) const;
    bool isItIn(Point point) const;
    void Afficher() const;

private:
    //attributs
    Point center;
    int diameter;
};

#endif // CERCLE_H
