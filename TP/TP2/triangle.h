#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <stdlib.h>
#include <cmath>
#include "point.h"
#include <array>
#define _USE_MATH_DEFINES

class Triangle{
public:
    //Constructeur
    Triangle(Point a, Point b, Point c);
    //getters & setters
    Point getA() const{
        return a;
    }
    Point getB() const{
        return b;
    }
    Point getC() const{
        return c;
    }

    void setA(Point _a){
        a=_a;
    }
    void setB(Point _b){
        b=_b;
    }
    void setC(Point _c){
        c=_c;
    }

    //Constante
    static const int NB_SIDE=3;

    //method
    float getBase() const;
    float getHigh() const;
    float getArea() const;
    float getPerimeter() const;
    std::array<float, NB_SIDE> getLengths() const;
    bool isIsocel() const;
    bool isRectangle() const;
    bool isEquilateral() const;
    void Afficher() const;

private:
    //attributs
    Point a;
    Point b;
    Point c;
};

#endif // TRIANGLE_H
