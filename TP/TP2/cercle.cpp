#include "cercle.h"

Cercle::Cercle(int _diametre, Point _center):center(_center), diameter(_diametre){

}

float Cercle::getPerimeter() const{
    return M_PI*getDiameter();
}

float Cercle::getArea() const{
    return M_PI * pow((getDiameter()/2),2);
}

float Cercle::calculDistanceFromCenter(Point point) const{
    return getCenter().calculDistance(point);
}

bool Cercle::isItOnBorder(Point point) const{

    if (calculDistanceFromCenter(point)==getDiameter()/2){
        return true;
    }
    else{
        return false;
    }
}

bool Cercle::isItIn(Point point) const{

    if (calculDistanceFromCenter(point)<getDiameter()){
        return true;
    }
    else{
        return false;
    }
}

void Cercle::Afficher() const{
    std::cout << "Centre du cercle : (" << getCenter().x <<", " << getCenter().y << ")\n"
                 "Diametre du cercle : " << getDiameter() << "\n"
                 "Surface du cercle :" << getArea() << "\n"
                 "Perimetre du cercle :" << getPerimeter() <<"\n"
                 "La classe cercle possede plusieur methode : \n"
                 "\t - isItOnBorder(Point point) pour savoir si un point est sur le contour du cercle,\n"
                 "\t - isItIn (Point point) pour savoir si un point est dans le cerlce \n"
                 "\t - calculDistanceFromCenter(Point point) pour connaitre la distance entre un point et le centredu cercle\n";
}
