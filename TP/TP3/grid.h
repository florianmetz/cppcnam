#ifndef GRID_H
#define GRID_H

#include <stdbool.h>
#include <vector>
#include <iostream>

class Grid
{
public:
    Grid(const int nbLine, const int nbColumn, const int nbTokenForVictory);
    void init();
    bool isEmpty(const int i, const int j) const;
    bool isFull() const;
    bool completeLine(const int i, const int player) const;
    bool completeColumn(const int j, const int player) const;
    void displayGrid() const;

    const std::vector<std::vector<int>> &getgrid() const{
        return grid;
    }
    void setgrid(const std::vector<std::vector<int>> &newgrid){
        grid=newgrid;
    }

    int getNB_COLUMN() const{
        return NB_COLUMN;
    }

protected:
    std::vector<std::vector<int>> grid;
    const int NB_LINE;
    const int NB_COLUMN;
private:
    const int NB_TOKEN_FOR_VICTORY;

};

#endif // GRID_H
