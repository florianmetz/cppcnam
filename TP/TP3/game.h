#ifndef GAME_H
#define GAME_H


#include "player.h"
#include "grid.h"
#include "gridmorpion.h"
#include "gridpower4.h"
#include <iostream>
#include <stdlib.h>
#include <string>


class Game
{
public:
    Game(Grid* _grid, int _choice);
    void gameOngoing();
    void gameStarting();
    bool caseChoose(const int player);
    bool chooseVerification(const int i) const;
    bool gameEnding();
    void congratulations(const int winner) const;
    void draw();

private:
    Player j1=Player(1);
    Player j2=Player(2);
    Grid* grid;
    int choice;
};


#endif // GAME_H
