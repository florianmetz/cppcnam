#ifndef GRIDPOWER4_H
#define GRIDPOWER4_H

#include "grid.h"

class GridPower4:public Grid
{
public:
    GridPower4();
    bool putToken(int j, int player);  //j numero de colonne dans laquelle on veut mettre un jeton
    bool completeDiagonal(int player) const;
    bool playerVictory(int player) const;
};

#endif // GRIDPOWER4_H
