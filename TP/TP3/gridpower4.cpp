#include "gridpower4.h"

GridPower4::GridPower4(): Grid(4,7,4) //4 lignes car imposé dans sujet
{
}


bool GridPower4::putToken(int j, int player){
    for(int i=NB_LINE-1;i>=0;i--){
        if(this->isEmpty(i,j)){
            this->grid[i][j]=player;
            return true;
        }
    }
    return false;
}

bool GridPower4::completeDiagonal(int player) const{
    // diagonal ascendente
    for (int i=3; i<NB_LINE; i++){
        for (int j=0; j<NB_COLUMN-3; j++){
            if (this->grid[i][j] == player && this->grid[i-1][j+1] == player && this->grid[i-2][j+2] == player && this->grid[i-3][j+3] == player)
                return true;
        }
    }
    // diagonal descendante
    for (int i=3; i<NB_LINE; i++){
        for (int j=3; j<NB_COLUMN; j++){
            if (this->grid[i][j] == player && this->grid[i-1][j-1] == player && this->grid[i-2][j-2] == player && this->grid[i-3][j-3] == player)
                return true;
        }
    }
    return false;
}


bool GridPower4::playerVictory(int player) const
{
    for(int i=0;i<NB_COLUMN;i++){
        if(completeColumn(i,player)){
            return true;
        }
    }
    for(int i=0;i<NB_LINE;i++){
        if(completeLine(i,player)){
            return true;
        }
    }

    return (completeDiagonal(player));
}
