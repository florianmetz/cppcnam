## Choix projet de départ TP 5

Nous avons choisi avec Maxime de repartir de ce projet, premièrement parce qu'il semble mieux fonctionner mais il respecte aussi un peu plus les principes SOLID

### S - Single Responsability

Ce projet à l'avantage d'avoir une classe grid mère qui est étendue par deux classes filles gridMorpion et gridPower4 donc ce principe est déjà partiellement en place. Par conséquent la classe gridMorpion s'occuper de la logique du jeu de morpion et gridPower4 de la logique du puissance 4.

### O - Open-Closed

L'architecture permet de faire des modifications facilement sans pour autant avoir à supprimer des méthodes. Si on veut ajouter une nouvelle méthode commune dans les différents jeux on pourra la créer dans la classe mère et ça ne changera pas le fonctionnement des classes filles pour autant. De la même manière on peut modifier les classes filles sans qu'il n'y ai d'impact sur une autre.

### L - Liskov Substitution

Ce principe est mis en place dans la dépendance entre grid et ses enfants. En effet chaque enfant de la classe grid peut utiliser des fonctions ou les réécrire pour les modifier au besoin.

### I - Interface Segregation

Dans aucun des deux projets il n'y avait d'interface, donc c'est impossible de juger pour ce point là. Il faudra en ajouter quand nécessaire pour le TP5 afin de respecter ce principe.

### D - Depedency inversion

Ce principe n'est implémenté dans aucun des deux projets, le but sera de faire des interfaces (classe abstraite pure). Il faudra expliquer le "quoi" (signature) avec ces interfaces, puis d'expliquer le "comment" (contenu) dans les classes qui en hériteront.

### Tell don't ask

Les deux projets respectent ce principe car aucun objet n'intéragit avec un autre pour obtenir une réponse sur son propre état.

### Loi de demeter

Grâce à l'héritage effectué pour recupérer les informations de la classe mère, on fait appel à la loi de demeter.
Exemple avec la classe Grid, elle est classe mère de Gridmorpion ainsi que de Gridpower4, on évite donc les risques de bug.

Autre exemple :
il faut éviter les écritures du type :

```cpp
string name = this -> Grid -> Joueur -> Nom
```

mais plutot stocker la valeur de grid puis la tester puis stocker la valeur de joueur puis la tester pour enfin être sur de récupérer le Nom.

# Choix pris pour la sauvegarde du Projet

Notre système de sauvegarde se trouve dans la classe Save, cette classe a comme attribut la référence d'un grille et possède deux méthodes, une pour sauvegarder, et l'autre pour charger des données.

Lors d'une partie il sera donc possible de sauvegarder la grille de jeu a n'importe quel moment, nous avons fait le choix de ne sauvegarder que les données de la grille et pas le nom des joueurs car c'est dans un premer temps plus facile comme ça.

Ainsi pour sauvegarder il faut faire de cette manière :

```cpp
Save s = Save(grid);
s.ecriture();
```

et de la même manière pour charger les données :

```cpp
Save s = Save(grid);
s.lecture();
```

Le chemin du fichier dans lequel on sauvegarde ou lit des données est constant et est définie dans save.h
Il n'est possible d'avoir qu'une seul sauvegarde pour le moment.
